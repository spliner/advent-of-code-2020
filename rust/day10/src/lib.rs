use std::collections::{HashSet, HashMap};

pub fn run(part: &str, input_path: &str) {
    let input = std::fs::read_to_string(input_path).unwrap();
    let adapters = parse_input(&input);

    if part == "part1" {
        let result = part1(&adapters);
        println!("{}", result);
    } else if part == "part2" {
        let result = part2(&adapters);
        println!("{}", result);
    }
}

fn part1(adapters: &Vec<i32>) -> i32 {
    let mut current_joltage = 0;
    let mut one_jolt_difference_count = 0;
    let mut three_jolts_difference_count = 1;
    let mut current_adapter = Some(0);

    while current_adapter.is_some() {
        let next_adapter = get_next_adapter(current_joltage, adapters);

        if next_adapter.is_some() {
            let adapter = next_adapter.unwrap();
            let difference = adapter - current_joltage;
            if difference == 1 {
                one_jolt_difference_count += 1;
            } else if difference == 3 {
                three_jolts_difference_count += 1;
            }

            current_joltage = adapter;
        }

        current_adapter = next_adapter;
    }

    one_jolt_difference_count * three_jolts_difference_count
}

fn part2(adapters: &Vec<i32>) -> i64 {
    count_adapters(0, adapters)
}

fn count_adapters(current_joltage: i32, adapters: &Vec<i32>) -> i64 {
    let mut cache: HashMap<i32, i64> = HashMap::new();

    fn do_count(j: i32, adapters: &Vec<i32>, cache: &mut HashMap<i32, i64>) -> i64 {
        match cache.get(&j) {
            Some(c) => *c,
            None => {
                let possible_adapters = get_possible_adapters(j, adapters);
                if possible_adapters.len() == 0 {
                    1
                } else {
                    let mut count: i64 = 0;
                    for joltage in possible_adapters {
                        count += do_count(joltage, adapters, cache);
                    }

                    cache.insert(j, count);

                    count
                }
            }
        }
    };

    do_count(current_joltage, adapters, &mut cache)
}

fn get_next_adapter(current_joltage: i32, adapters: &Vec<i32>) -> Option<i32> {
    let possible_adapters = get_possible_adapters(current_joltage, adapters);
    possible_adapters.iter().min().map(|i| *i)
}

fn get_possible_adapters(current_joltage: i32, adapters: &Vec<i32>) -> HashSet<i32> {
    let accepted_joltages = [
        current_joltage + 1,
        current_joltage + 2,
        current_joltage + 3,
    ]
    .iter()
    .cloned()
    .collect::<HashSet<_>>();

    adapters
        .iter()
        .filter(|a| accepted_joltages.contains(a))
        .cloned()
        .collect()
}

fn parse_input(input: &str) -> Vec<i32> {
    input
        .lines()
        .map(|l| l.trim())
        .map(|l| l.parse::<i32>().unwrap())
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_aoc_input() {
        let input = "16
10
15
5
1
11
7
19
6
12
4";

        let adapters = parse_input(input);

        assert_eq!(adapters.len(), 11);
        assert_eq!(adapters[0], 16);
        assert_eq!(adapters[10], 4);
    }

    #[test]
    fn get_next_adapter_1() {
        let input = "16
10
15
5
1
11
7
19
6
12
4";
        let adapters = parse_input(input);

        let next_adapter = get_next_adapter(0, &adapters);

        assert!(next_adapter.is_some());
        assert_eq!(next_adapter.unwrap(), 1);
    }

    #[test]
    fn get_next_adapter_2() {
        let input = "16
10
15
5
1
11
7
19
6
12
4";
        let adapters = parse_input(input);

        let next_adapter = get_next_adapter(1, &adapters);

        assert!(next_adapter.is_some());
        assert_eq!(next_adapter.unwrap(), 4);
    }

    #[test]
    fn get_next_adapter_3() {
        let input = "16
10
15
5
1
11
7
19
6
12
4";
        let adapters = parse_input(input);

        let next_adapter = get_next_adapter(4, &adapters);

        assert!(next_adapter.is_some());
        assert_eq!(next_adapter.unwrap(), 5);
    }

    #[test]
    fn part1_aoc_input() {
        let input = "16
10
15
5
1
11
7
19
6
12
4";
        let adapters = parse_input(input);

        let result = part1(&adapters);

        assert_eq!(result, 35);
    }

    #[test]
    fn count_adapters_aoc_input() {
        let input = "16
10
15
5
1
11
7
19
6
12
4";
        let adapters = parse_input(input);

        let result = count_adapters(0, &adapters);

        assert_eq!(result, 8);
    }

    #[test]
    fn count_adapters_aoc_input_2() {
        let input = "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";
        let adapters = parse_input(input);

        let result = count_adapters(0, &adapters);

        assert_eq!(result, 19208);
    }

    #[test]
    fn part2_aoc_input_2() {
        let input = "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";
        let adapters = parse_input(input);

        let result = part2(&adapters);

        assert_eq!(result, 19208);
    }
}
