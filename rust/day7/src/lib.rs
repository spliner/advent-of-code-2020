use std::collections::{HashMap, HashSet};

pub fn run(part: &str, input_path: &str) {
    let input = std::fs::read_to_string(input_path).unwrap();
    let map = parse_input(&input);

    if part == "part1" {
        let result = part1("shiny gold", &map);
        println!("{}", result.len());
    } else if part == "part2" {
        let count = part2("shiny gold", &map);
        println!("{}", count);
    }
}

fn part1(color: &str, map: &HashMap<String, HashMap<String, i32>>) -> HashSet<String> {
    let valid_colors = map
        .iter()
        .filter(|(_, m)| m.contains_key(color))
        .map(|(c, _)| c)
        .cloned()
        .collect::<HashSet<_>>();

    let mut result = valid_colors.clone();
    for color in valid_colors {
        let colors = part1(&color, map);
        result = result.union(&colors).cloned().collect::<HashSet<_>>();
    }

    result
}

fn part2(color: &str, map: &HashMap<String, HashMap<String, i32>>) -> i32 {
    let bags = map.get(color);
    match bags {
        Some(b) => {
            let mut total = 0;
            for (color, count) in b {
                total += count;
                total += count * part2(color, map);
            }
            total
        }
        None => 0,
    }
}

fn parse_input(input: &str) -> HashMap<String, HashMap<String, i32>> {
    input
        .lines()
        .map(|l| parse_line(l))
        .collect::<HashMap<_, _>>()
}

fn parse_line(line: &str) -> (String, HashMap<String, i32>) {
    let split = line.split(" bags contain ").collect::<Vec<_>>();
    let color = split[0];

    let other_bags = match split[1] {
        "no other bags." => HashMap::new(),
        _ => split[1]
            .split(", ")
            .map(|s| {
                s.trim()
                    .replace(".", "")
                    .replace(" bags", "")
                    .replace(" bag", "")
            })
            .map(|s| {
                let split = s.splitn(2, " ").collect::<Vec<_>>();
                let color = split[1];
                let count = split[0];

                (String::from(color), count.parse::<i32>().unwrap())
            })
            .collect::<HashMap<_, _>>(),
    };

    (String::from(color), other_bags)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_line_aoc_input_1() {
        let line = "light red bags contain 1 bright white bag, 2 muted yellow bags.";

        let (color, other_bags) = parse_line(line);

        assert_eq!(color, "light red");
        assert_eq!(other_bags.len(), 2);

        let bag = other_bags.get("bright white");
        assert!(bag.is_some());
        assert_eq!(*bag.unwrap(), 1);

        let bag = other_bags.get("muted yellow");
        assert!(bag.is_some());
        assert_eq!(*bag.unwrap(), 2);
    }

    #[test]
    fn parse_line_aoc_input_2() {
        let line = "bright white bags contain 1 shiny gold bag.";

        let (color, other_bags) = parse_line(line);

        assert_eq!(color, "bright white");
        assert_eq!(other_bags.len(), 1);

        let bag = other_bags.get("shiny gold");
        assert!(bag.is_some());
        assert_eq!(*bag.unwrap(), 1);
    }

    #[test]
    fn parse_line_aoc_input_3() {
        let line = "faded blue bags contain no other bags.";

        let (color, other_bags) = parse_line(line);

        assert_eq!(color, "faded blue");
        assert_eq!(other_bags.len(), 0);
    }

    #[test]
    fn parse_aoc_input() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";

        let map = parse_input(input);

        assert_eq!(map.len(), 9);
    }

    #[test]
    fn part1_aoc_input() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
        let map = parse_input(input);

        let result = part1("shiny gold", &map);

        assert_eq!(result.len(), 4);
    }

    #[test]
    fn part2_aoc_input_1() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
        let map = parse_input(input);

        let count = part2("shiny gold", &map);

        assert_eq!(count, 32);
    }

    #[test]
    fn part2_aoc_input_2() {
        let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
        let map = parse_input(input);

        let count = part2("shiny gold", &map);

        assert_eq!(count, 126);
    }
}
