use std::collections::HashSet;

#[derive(Debug, PartialEq)]
struct Group {
    answers: Vec<String>,
}

impl Group {
    fn new(answers: Vec<String>) -> Self {
        Group { answers }
    }

    fn count_any_yes(&self) -> usize {
        self.answers
            .iter()
            .flat_map(|a| a.chars())
            .collect::<HashSet<char>>()
            .len()
    }

    fn count_all_yes(&self) -> usize {
        let mut all_yes = self.answers[0].chars().collect::<HashSet<_>>();
        for i in 1..self.answers.len() {
            let answers = self.answers[i].chars().collect::<HashSet<_>>();
            all_yes = all_yes
                .intersection(&answers)
                .cloned()
                .collect::<HashSet<_>>();
        }
        all_yes.len()
    }
}

pub fn run(part: &str, input_path: &str) {
    let input = std::fs::read_to_string(input_path).unwrap();
    let groups = parse_input(&input);

    if part == "part1" {
        let sum = part1(&groups);
        println!("{}", sum);
    } else if part == "part2" {
        let sum = part2(&groups);
        println!("{}", sum);
    }
}

fn part1(groups: &Vec<Group>) -> usize {
    groups.iter().map(|g| g.count_any_yes()).sum()
}

fn part2(groups: &Vec<Group>) -> usize {
    groups.iter().map(|g| g.count_all_yes()).sum()
}

fn parse_input(input: &str) -> Vec<Group> {
    let lines = input.lines().map(|l| l.trim()).collect::<Vec<&str>>();
    let mut groups: Vec<Group> = Vec::new();
    let mut current_answers: Vec<String> = Vec::new();
    for line in lines {
        if line.is_empty() {
            let group = Group::new(current_answers.clone());
            groups.push(group);
            current_answers.clear();
            continue;
        }

        current_answers.push(String::from(line));
    }

    if current_answers.len() > 0 {
        let group = Group::new(current_answers.clone());
        groups.push(group);
    }

    groups
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn count_any_yes_aoc_group_1() {
        let answers = vec![String::from("abc")];
        let group = Group::new(answers);

        let count = group.count_any_yes();

        assert_eq!(count, 3);
    }

    #[test]
    fn count_any_yes_aoc_group_2() {
        let answers = vec![String::from("a"), String::from("b"), String::from("c")];
        let group = Group::new(answers);

        let count = group.count_any_yes();

        assert_eq!(count, 3);
    }

    #[test]
    fn count_any_yes_aoc_group_3() {
        let answers = vec![String::from("ab"), String::from("ac")];
        let group = Group::new(answers);

        let count = group.count_any_yes();

        assert_eq!(count, 3);
    }

    #[test]
    fn count_any_yes_aoc_group_4() {
        let answers = vec![
            String::from("a"),
            String::from("a"),
            String::from("a"),
            String::from("a"),
        ];
        let group = Group::new(answers);

        let count = group.count_any_yes();

        assert_eq!(count, 1);
    }

    #[test]
    fn count_any_yes_aoc_group_5() {
        let answers = vec![String::from("b")];
        let group = Group::new(answers);

        let count = group.count_any_yes();

        assert_eq!(count, 1);
    }

    #[test]
    fn parse_aoc_input() {
        let input = "abc

a
b
c

ab
ac

a
a
a
a

b";

        let groups = parse_input(input);

        assert_eq!(groups.len(), 5);
        assert_eq!(groups[0].answers.len(), 1);
        assert_eq!(groups[0].count_any_yes(), 3);
        assert_eq!(groups[1].answers.len(), 3);
        assert_eq!(groups[1].count_any_yes(), 3);
        assert_eq!(groups[2].answers.len(), 2);
        assert_eq!(groups[2].count_any_yes(), 3);
        assert_eq!(groups[3].answers.len(), 4);
        assert_eq!(groups[3].count_any_yes(), 1);
        assert_eq!(groups[4].answers.len(), 1);
        assert_eq!(groups[4].count_any_yes(), 1);
    }

    #[test]
    fn part1_aoc_input() {
        let input = "abc

a
b
c

ab
ac

a
a
a
a

b";
        let groups = parse_input(input);

        let sum = part1(&groups);

        assert_eq!(sum, 11);
    }

    #[test]
    fn count_all_yes_aoc_group_1() {
        let answers = vec![String::from("abc")];
        let group = Group::new(answers);

        let count = group.count_all_yes();

        assert_eq!(count, 3);
    }

    #[test]
    fn count_all_yes_aoc_group_2() {
        let answers = vec![String::from("a"), String::from("b"), String::from("c")];
        let group = Group::new(answers);

        let count = group.count_all_yes();

        assert_eq!(count, 0);
    }

    #[test]
    fn count_all_yes_aoc_group_3() {
        let answers = vec![String::from("ab"), String::from("ac")];
        let group = Group::new(answers);

        let count = group.count_all_yes();

        assert_eq!(count, 1);
    }

    #[test]
    fn count_all_yes_aoc_group_4() {
        let answers = vec![
            String::from("a"),
            String::from("a"),
            String::from("a"),
            String::from("a"),
        ];
        let group = Group::new(answers);

        let count = group.count_all_yes();

        assert_eq!(count, 1);
    }

    #[test]
    fn count_all_yes_aoc_group_5() {
        let answers = vec![String::from("b")];
        let group = Group::new(answers);

        let count = group.count_all_yes();

        assert_eq!(count, 1);
    }

    #[test]
    fn part2_aoc_input() {
        let input = "abc

a
b
c

ab
ac

a
a
a
a

b";
        let groups = parse_input(input);

        let sum = part2(&groups);

        assert_eq!(sum, 6);
    }
}
