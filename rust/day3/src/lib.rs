#[derive(Debug, PartialEq)]
enum PositionType {
    OpenSpace,
    Tree,
    Unknown,
}

#[derive(Debug, PartialEq)]
struct Line {
    chars: Vec<char>,
}

impl Line {
    fn new(line: &str) -> Self {
        let chars = line.chars().collect::<Vec<char>>();
        Self { chars }
    }

    fn get_position_type(&self, i: usize) -> PositionType {
        let len = self.chars.len();
        let index = i % len;
        match self.chars[index] {
            '.' => PositionType::OpenSpace,
            '#' => PositionType::Tree,
            _ => PositionType::Unknown,
        }
    }
}

type Grid = Vec<Line>;

pub fn run(part: &str, input_path: &str) {
    let raw_grid = std::fs::read_to_string(input_path).unwrap();
    let grid = parse_grid(&raw_grid);
    if part == "part1" {
        let tree_count = part1(&grid);
        println!("{}", tree_count);
    } else if part == "part2" {
        let result = part2(&grid);
        println!("{}", result);
    }
}

fn parse_grid(input: &str) -> Grid {
    input
        .lines()
        .map(|l| l.trim())
        .filter(|l| !l.is_empty())
        .map(|l| Line::new(l))
        .collect()
}

fn part1(grid: &Grid) -> i32 {
    count_trees(grid, 3, 1)
}

fn count_trees(grid: &Grid, step_x: usize, step_y: usize) -> i32 {
    let mut tree_count = 0;
    let mut current_x = step_x;
    let mut current_y: usize = step_y;

    while current_y < grid.len() {
        let line = &grid[current_y];
        if line.get_position_type(current_x) == PositionType::Tree {
            tree_count += 1;
        }

        current_x += step_x;
        current_y += step_y;
    }

    tree_count
}

fn part2(grid: &Grid) -> i32 {
    let slopes = vec![
        (1usize, 1usize),
        (3usize, 1usize),
        (5usize, 1usize),
        (7usize, 1usize),
        (1usize, 2usize)
    ];

    slopes
        .iter()
        .map(|(x, y)| count_trees(grid, *x, *y))
        .fold(1, |acc, c| acc * c)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn line_position_types() {
        let raw_line = "..##.";

        let line = Line::new(raw_line);

        assert_eq!(line.chars.len(), raw_line.len());
        assert_eq!(line.get_position_type(0), PositionType::OpenSpace);
        assert_eq!(line.get_position_type(1), PositionType::OpenSpace);
        assert_eq!(line.get_position_type(2), PositionType::Tree);
        assert_eq!(line.get_position_type(3), PositionType::Tree);
        assert_eq!(line.get_position_type(4), PositionType::OpenSpace);

        assert_eq!(line.get_position_type(5), PositionType::OpenSpace);
        assert_eq!(line.get_position_type(6), PositionType::OpenSpace);
        assert_eq!(line.get_position_type(7), PositionType::Tree);
        assert_eq!(line.get_position_type(8), PositionType::Tree);
        assert_eq!(line.get_position_type(9), PositionType::OpenSpace);
    }

    #[test]
    fn parse_grid_aoc_input() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";

        let grid = parse_grid(input);

        assert_eq!(grid.len(), 11);
        let lines_with_correct_size = grid.iter().filter(|l| l.chars.len() == 11).count();
        assert_eq!(lines_with_correct_size, 11)
    }

    #[test]
    fn part1_aoc_input() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";
        let grid = parse_grid(input);

        let tree_count = part1(&grid);

        assert_eq!(tree_count, 7);
    }

    #[test]
    fn count_trees_right_1_down_1() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";
        let grid = parse_grid(input);

        let tree_count = count_trees(&grid, 1usize, 1usize);

        assert_eq!(tree_count, 2);
    }

    #[test]
    fn count_trees_right_3_down_1() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";
        let grid = parse_grid(input);

        let tree_count = count_trees(&grid, 3usize, 1usize);

        assert_eq!(tree_count, 7);
    }

    #[test]
    fn count_trees_right_5_down_1() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";
        let grid = parse_grid(input);

        let tree_count = count_trees(&grid, 5usize, 1usize);

        assert_eq!(tree_count, 3);
    }

    #[test]
    fn count_trees_right_7_down_1() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";
        let grid = parse_grid(input);

        let tree_count = count_trees(&grid, 7usize, 1usize);

        assert_eq!(tree_count, 4);
    }

    #[test]
    fn count_trees_right_1_down_2() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";
        let grid = parse_grid(input);

        let tree_count = count_trees(&grid, 1usize, 2usize);

        assert_eq!(tree_count, 2);
    }

    #[test]
    fn part2_aoc_input() {
        let input = "..##.......\n\
                           #...#...#..\n\
                           .#....#..#.\n\
                           ..#.#...#.#\n\
                           .#...##..#.\n\
                           ..#.##.....\n\
                           .#.#.#....#\n\
                           .#........#\n\
                           #.##...#...\n\
                           #...##....#\n\
                           .#..#...#.#";
        let grid = parse_grid(input);

        let result = part2(&grid);

        assert_eq!(result, 336);
    }
}
