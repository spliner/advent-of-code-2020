const TARGET_VALUE: i32 = 2020;

pub fn run(part: String, input_path: String) {
    let entries = get_entries(&input_path);

    if part == "part1" {
        let result = part1(&entries);
        match result {
            Some(i) => println!("{}", i),
            None => println!("No combination of numbers match {}", TARGET_VALUE),
        }
    } else if part == "part2" {
        let result = part2(&entries);
        match result {
            Some(i) => println!("{}", i),
            None => println!("No combination of numbers match {}", TARGET_VALUE),
        }
    }
}

fn get_entries(input_path: &str) -> Vec<i32> {
    let contents = std::fs::read_to_string(input_path).unwrap();

    contents
        .lines()
        .map(|l| l.trim())
        .filter(|l| !l.is_empty())
        .map(|l| l.parse::<i32>().unwrap())
        .collect()
}

fn part1(entries: &Vec<i32>) -> Option<i32> {
    for i in 0..entries.len() {
        let x = entries[i];

        for j in (i + i)..entries.len() {
            let y = entries[j];

            if x + y == TARGET_VALUE {
                return Some(x * y);
            }
        }
    }

    None
}

fn part2(entries: &Vec<i32>) -> Option<i32> {
    for i in 0..entries.len() {
        let x = entries[i];

        for j in (i + 1)..entries.len() {
            let y = entries[j];

            for k in (j + 1)..entries.len() {
                let z = entries[k];

                if x + y + z == TARGET_VALUE {
                    return Some(x * y * z);
                }
            }
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn aoc_part1_input() {
        let entries = vec![
            1721,
            979,
            366,
            299,
            675,
            1456,
        ];

        let result = part1(&entries);
        assert_eq!(result.is_some(), true);
        assert_eq!(result.unwrap(), 514579);
    }

    #[test]
    fn part1_no_matches_should_return_none() {
        let entries = vec![1, 2, 3];

        let result = part1(&entries);
        assert_eq!(result.is_none(), true);
    }

    #[test]
    fn aoc_part2_input() {
        let entries = vec![
            1721,
            979,
            366,
            299,
            675,
            1456,
        ];

        let result = part2(&entries);
        assert_eq!(result.is_some(), true);
        assert_eq!(result.unwrap(), 241861950);
    }

    #[test]
    fn part2_input2() {
        let entries = vec![
            9999,
            2017,
            1,
            2,
        ];

        let result = part2(&entries);
        assert_eq!(result.is_some(), true);
        assert_eq!(result.unwrap(), 4034);
    }

    #[test]
    fn part2_no_matches_should_return_none() {
        let entries = vec![1, 2, 3];

        let result = part2(&entries);
        assert_eq!(result.is_none(), true);
    }
}
