use std::collections::HashSet;

pub fn run(part: &str, input_path: &str) {
    let specifications = std::fs::read_to_string(input_path)
        .unwrap()
        .lines()
        .map(|l| l.trim())
        .filter(|l| !l.is_empty())
        .map(|l| String::from(l))
        .collect::<Vec<String>>();

    if part == "part1" {
        let max_id = part1(&specifications, 128, 8);
        println!("{}", max_id);
    } else if part == "part2" {
        let missing_id = part2(&specifications, 128, 8);
        println!("{}", missing_id);
    }
}

fn part1(specifications: &Vec<String>, rows: i32, columns: i32) -> i32 {
    specifications
        .iter()
        .map(|s| get_seat(s, rows, columns))
        .map(|s| get_seat_id(&s))
        .max()
        .unwrap()
}

fn part2(specifications: &Vec<String>, rows: i32, columns: i32) -> i32 {
    let seat_ids = specifications
        .iter()
        .map(|s| get_seat(s, rows, columns))
        .map(|s| get_seat_id(&s))
        .collect::<HashSet<i32>>();
    let min_seat_id = seat_ids
        .iter()
        .min()
        .unwrap();
    let max_seat_id = seat_ids
        .iter()
        .max()
        .unwrap();
    let plane_seat_ids = (*min_seat_id..=*max_seat_id).map(|i| i).collect::<HashSet<i32>>();
    *plane_seat_ids.difference(&seat_ids).nth(0).unwrap()
}

fn get_seat(specification: &str, rows: i32, columns: i32) -> (i32, i32) {
    let mut min_row = 0;
    let mut max_row = rows - 1;
    let mut min_column = 0;
    let mut max_column = columns - 1;

    for c in specification.chars() {
        let row_len = max_row - min_row + 1;
        let row_step = row_len / 2;
        let column_len = max_column - min_column + 1;
        let column_step = column_len / 2;
        match c {
            'F' => max_row = max_row - row_step,
            'B' => min_row = min_row + row_step,
            'L' => max_column = max_column - column_step,
            'R' => min_column = min_column + column_step,
            _ => panic!("Unexpected specification")
        }
    }

    (min_row, min_column)
}

fn get_seat_id(seat: &(i32, i32)) -> i32 {
    let (row, column) = seat;
    row * 8 + column
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_seat_aoc_input_1() {
        let specification = "FBFBBFFRLR";

        let seat = get_seat(specification, 128, 8);

        assert_eq!(seat, (44, 5));
    }
    #[test]
    fn get_seat_aoc_input_2() {
        let specification = "BFFFBBFRRR";

        let seat = get_seat(specification, 128, 8);

        assert_eq!(seat, (70, 7));
    }

    #[test]
    fn get_seat_aoc_input_3() {
        let specification = "FFFBBBFRRR";

        let seat = get_seat(specification, 128, 8);

        assert_eq!(seat, (14, 7));
    }

    #[test]
    fn get_seat_aoc_input_4() {
        let specification = "BBFFBBFRLL";

        let seat = get_seat(specification, 128, 8);

        assert_eq!(seat, (102, 4));
    }

    #[test]
    fn part1_aoc_input() {
        let specifications = vec![
            String::from("FBFBBFFRLR"),
            String::from("BFFFBBFRRR"),
            String::from("FFFBBBFRRR"),
            String::from("BBFFBBFRLL"),
        ];

        let max_id = part1(&specifications, 128, 8);

        assert_eq!(max_id, 820);
    }
}
