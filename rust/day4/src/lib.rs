#[macro_use]
extern crate lazy_static;
use regex::Regex;
use std::collections::{HashMap, HashSet};

#[derive(Debug)]
struct Passport {
    birth_year: Option<String>,
    issue_year: Option<String>,
    expiration_year: Option<String>,
    height: Option<String>,
    hair_color: Option<String>,
    eye_color: Option<String>,
    passport_id: Option<String>,
    country_id: Option<String>,
}

pub fn run(part: &str, input_path: &str) {
    let input = std::fs::read_to_string(input_path).unwrap();
    let passports = parse_input(&input);

    if part == "part1" {
        let valid_passports = part1(&passports);
        println!("{}", valid_passports);
    } else if part == "part2" {
        let valid_passports = part2(&passports);
        println!("{}", valid_passports);
    }
}

fn part1(passports: &Vec<Passport>) -> usize {
    let is_valid = |p: &Passport| {
        p.birth_year.is_some()
            && p.issue_year.is_some()
            && p.expiration_year.is_some()
            && p.height.is_some()
            && p.hair_color.is_some()
            && p.eye_color.is_some()
            && p.passport_id.is_some()
    };

    passports.iter().filter(|p| is_valid(*p)).count()
}

fn part2(passports: &Vec<Passport>) -> usize {
    let is_range_valid = |o: &Option<String>, len, min, max| match o {
        Some(s) => {
            s.len() == len
                && match s.parse::<i32>() {
                    Ok(i) => i >= min && i <= max,
                    _ => false,
                }
        }
        _ => false,
    };

    let is_height_valid = |o: &Option<String>| match o {
        Some(h) => {
            let value = h.chars().take(h.len() - 2).collect::<String>();
            match value.parse::<i32>() {
                Ok(v) => {
                    let unit = h
                        .chars()
                        .rev()
                        .take(2)
                        .collect::<Vec<_>>()
                        .into_iter()
                        .rev()
                        .collect::<String>();
                    match unit.as_str() {
                        "cm" => v >= 150 && v <= 193,
                        "in" => v >= 59 && v <= 76,
                        _ => false,
                    }
                }
                _ => false,
            }
        }
        _ => false,
    };

    lazy_static! {
        static ref HAIR_COLOR_REGEX: Regex = Regex::new(r"^#[0-9a-f]+$").unwrap();
    }
    let is_hair_color_valid = |o: &Option<String>| match o {
        Some(c) => HAIR_COLOR_REGEX.is_match(c),
        _ => false,
    };

    let valid_eye_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
        .iter()
        .cloned()
        .collect::<HashSet<&str>>();
    let is_eye_color_valid = |o: &Option<String>| match o {
        Some(c) => valid_eye_colors.contains(c.as_str()),
        _ => false,
    };

    let is_passport_id_valid = |o: &Option<String>| {
        o.as_ref()
            .map_or(false, |i| i.len() == 9 && i.parse::<i32>().is_ok())
    };

    let is_valid = |p: &Passport| {
        is_range_valid(&p.birth_year, 4, 1920, 2002)
            && is_range_valid(&p.issue_year, 4, 2010, 2020)
            && is_range_valid(&p.expiration_year, 4, 2020, 2030)
            && is_height_valid(&p.height)
            && is_hair_color_valid(&p.hair_color)
            && is_eye_color_valid(&p.eye_color)
            && is_passport_id_valid(&p.passport_id)
    };

    passports.iter().filter(|p| is_valid(*p)).count()
}

fn parse_input(input: &str) -> Vec<Passport> {
    let mut current_chunk = String::new();
    let mut passports = Vec::new();

    for line in input.lines() {
        if line.is_empty() {
            let passport = parse_chunk(&current_chunk);
            passports.push(passport);

            current_chunk.clear();

            continue;
        }

        current_chunk.push_str(line);
        current_chunk.push_str("\n");
    }

    let passport = parse_chunk(&current_chunk);
    passports.push(passport);

    passports
}

fn parse_chunk(chunk: &str) -> Passport {
    let fields = chunk
        .split("\n")
        .flat_map(|s| s.split(" "))
        .filter_map(|f| {
            let mut split = f.split(":");
            match (split.next(), split.next()) {
                (Some(n), Some(v)) => Some((n, v)),
                _ => None,
            }
        })
        .collect::<HashMap<_, _>>();

    let get_value = |k: &str| fields.get(k).map(|s| String::from(s.trim()));
    Passport {
        birth_year: get_value("byr"),
        issue_year: get_value("iyr"),
        expiration_year: get_value("eyr"),
        height: get_value("hgt"),
        hair_color: get_value("hcl"),
        eye_color: get_value("ecl"),
        passport_id: get_value("pid"),
        country_id: get_value("cid"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_chunk_1() {
        let chunk = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\n\
                           byr:1937 iyr:2017 cid:147 hgt:183cm";

        let passport = parse_chunk(chunk);

        assert_eq!(passport.birth_year.is_some(), true);
        assert_eq!(passport.birth_year.unwrap(), String::from("1937"));
        assert_eq!(passport.issue_year.is_some(), true);
        assert_eq!(passport.issue_year.unwrap(), String::from("2017"));
        assert_eq!(passport.expiration_year.is_some(), true);
        assert_eq!(passport.expiration_year.unwrap(), String::from("2020"));
        assert_eq!(passport.height.is_some(), true);
        assert_eq!(passport.height.unwrap(), String::from("183cm"));
        assert_eq!(passport.hair_color.is_some(), true);
        assert_eq!(passport.hair_color.unwrap(), String::from("#fffffd"));
        assert_eq!(passport.eye_color.is_some(), true);
        assert_eq!(passport.eye_color.unwrap(), String::from("gry"));
        assert_eq!(passport.passport_id.is_some(), true);
        assert_eq!(passport.passport_id.unwrap(), String::from("860033327"));
        assert_eq!(passport.country_id.is_some(), true);
        assert_eq!(passport.country_id.unwrap(), String::from("147"));
    }

    #[test]
    fn parse_chunk_2() {
        let chunk = "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n\
                           hcl:#cfa07d byr:1929";

        let passport = parse_chunk(chunk);

        assert_eq!(passport.birth_year.is_some(), true);
        assert_eq!(passport.birth_year.unwrap(), String::from("1929"));
        assert_eq!(passport.issue_year.is_some(), true);
        assert_eq!(passport.issue_year.unwrap(), "2013");
        assert_eq!(passport.expiration_year.is_some(), true);
        assert_eq!(passport.expiration_year.unwrap(), String::from("2023"));
        assert_eq!(passport.height.is_none(), true);
        assert_eq!(passport.hair_color.is_some(), true);
        assert_eq!(passport.hair_color.unwrap(), String::from("#cfa07d"));
        assert_eq!(passport.eye_color.is_some(), true);
        assert_eq!(passport.eye_color.unwrap(), "amb");
        assert_eq!(passport.passport_id.is_some(), true);
        assert_eq!(passport.passport_id.unwrap(), String::from("028048884"));
        assert_eq!(passport.country_id.is_some(), true);
        assert_eq!(passport.country_id.unwrap(), String::from("350"));
    }

    #[test]
    fn parse_chunk_3() {
        let chunk = "hcl:#ae17e1 iyr:2013\n\
                           eyr:2024\n\
                           ecl:brn pid:760753108 byr:1931\n\
                           hgt:179cm";

        let passport = parse_chunk(chunk);

        assert_eq!(passport.birth_year.is_some(), true);
        assert_eq!(passport.birth_year.unwrap(), String::from("1931"));
        assert_eq!(passport.issue_year.is_some(), true);
        assert_eq!(passport.issue_year.unwrap(), String::from("2013"));
        assert_eq!(passport.expiration_year.is_some(), true);
        assert_eq!(passport.expiration_year.unwrap(), String::from("2024"));
        assert_eq!(passport.height.is_some(), true);
        assert_eq!(passport.height.unwrap(), String::from("179cm"));
        assert_eq!(passport.hair_color.is_some(), true);
        assert_eq!(passport.hair_color.unwrap(), String::from("#ae17e1"));
        assert_eq!(passport.eye_color.is_some(), true);
        assert_eq!(passport.eye_color.unwrap(), String::from("brn"));
        assert_eq!(passport.passport_id.is_some(), true);
        assert_eq!(passport.passport_id.unwrap(), String::from("760753108"));
        assert_eq!(passport.country_id.is_none(), true);
    }

    #[test]
    fn parse_chunk_4() {
        let chunk = "hcl:#cfa07d eyr:2025 pid:166559648\n\
                           iyr:2011 ecl:brn hgt:59in";

        let passport = parse_chunk(chunk);

        assert_eq!(passport.birth_year.is_none(), true);
        assert_eq!(passport.issue_year.is_some(), true);
        assert_eq!(passport.issue_year.unwrap(), String::from("2011"));
        assert_eq!(passport.expiration_year.is_some(), true);
        assert_eq!(passport.expiration_year.unwrap(), String::from("2025"));
        assert_eq!(passport.height.is_some(), true);
        assert_eq!(passport.height.unwrap(), String::from("59in"));
        assert_eq!(passport.hair_color.is_some(), true);
        assert_eq!(passport.hair_color.unwrap(), String::from("#cfa07d"));
        assert_eq!(passport.eye_color.is_some(), true);
        assert_eq!(passport.eye_color.unwrap(), String::from("brn"));
        assert_eq!(passport.passport_id.is_some(), true);
        assert_eq!(passport.passport_id.unwrap(), String::from("166559648"));
        assert_eq!(passport.country_id.is_none(), true);
    }

    #[test]
    fn parse_aoc_input() {
        let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
                           byr:1937 iyr:2017 cid:147 hgt:183cm\n\
                           \n\
                           iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n\
                           hcl:#cfa07d byr:1929\n\
                           \n\
                           hcl:#ae17e1 iyr:2013\n\
                           eyr:2024\n\
                           ecl:brn pid:760753108 byr:1931\n\
                           hgt:179cm\n\
                           \n\
                           hcl:#cfa07d eyr:2025 pid:166559648\n\
                           iyr:2011 ecl:brn hgt:59in";

        let passwords = parse_input(input);

        assert_eq!(passwords.len(), 4);
    }

    #[test]
    fn part1_aoc_input() {
        let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
                           byr:1937 iyr:2017 cid:147 hgt:183cm\n\
                           \n\
                           iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n\
                           hcl:#cfa07d byr:1929\n\
                           \n\
                           hcl:#ae17e1 iyr:2013\n\
                           eyr:2024\n\
                           ecl:brn pid:760753108 byr:1931\n\
                           hgt:179cm\n\
                           \n\
                           hcl:#cfa07d eyr:2025 pid:166559648\n\
                           iyr:2011 ecl:brn hgt:59in";

        let passports = parse_input(input);
        let valid_passports = part1(&passports);

        assert_eq!(valid_passports, 2);
    }

    #[test]
    fn part2_invalid_passports() {
        let input = "eyr:1972 cid:100
                           hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926\n\
                           \n\
                           iyr:2019\n\
                           hcl:#602927 eyr:1967 hgt:170cm\n\
                           ecl:grn pid:012533040 byr:1946\n\
                           \n\
                           hcl:dab227 iyr:2012\n\
                           ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277\n\
                           \n\
                           hgt:59cm ecl:zzz\n\
                           eyr:2038 hcl:74454a iyr:2023\n\
                           pid:3556412378 byr:2007";

        let passports = parse_input(input);
        let valid_passports = part2(&passports);

        assert_eq!(valid_passports, 0);
    }

    #[test]
    fn part2_valid_passports() {
        let input = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
                           hcl:#623a2f\n\
                           \n\
                           eyr:2029 ecl:blu cid:129 byr:1989\n\
                           iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm\n\
                           \n\
                           hcl:#888785\n\
                           hgt:164cm byr:2001 iyr:2015 cid:88\n\
                           pid:545766238 ecl:hzl\n\
                           eyr:2022\n\
                           \n\
                           iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";

        let passports = parse_input(input);
        let valid_passports = part2(&passports);

        assert_eq!(valid_passports, 4);
    }
}
