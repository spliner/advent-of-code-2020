fn main() {
    let part = std::env::args().nth(1).expect("No part given");
    let input_path = std::env::args().nth(2).expect("No path given");

    day4::run(&part, &input_path);
}
