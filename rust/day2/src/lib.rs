#[macro_use] extern crate lazy_static;
use regex::Regex;

type PasswordEntry = (usize, usize, char, String);

pub fn run(part: String, input_path: String) {
    let entries = get_entries(&input_path);

    if part == "part1" {
        let count = part1(&entries);
        println!("{}", count);
    } else if part == "part2" {
        let count = part2(&entries);
        println!("{}", count);
    }
}

fn get_entries(input_path: &str) -> Vec<PasswordEntry> {
    let contents = std::fs::read_to_string(input_path).unwrap();

    contents
        .lines()
        .map(|l| l.trim())
        .filter(|l| !l.is_empty())
        .map(|l| parse_line(l))
        .filter(|e| e.is_some())
        .map(|e| e.unwrap())
        .collect()
}

fn part1(entries: &Vec<PasswordEntry>) -> usize {
    entries
        .iter()
        .filter(|e| is_part1_valid(e))
        .count()
}

fn is_part1_valid(entry: &PasswordEntry) -> bool {
    let (low, high, char, password) = entry;
    let count = password
        .chars()
        .filter(|c| c == char)
        .count();

    count >= *low && count <= *high
}

fn part2(entries: &Vec<PasswordEntry>) -> usize {
    entries
        .iter()
        .filter(|e| is_part2_valid(e))
        .count()
}

fn is_part2_valid(entry: &PasswordEntry) -> bool {
    let (position1, position2, char, password) = entry;
    let position1 = position1 - 1;
    let position2 = position2 - 1;

    let chars: Vec<char> = password.chars().collect();
    if position1 < chars.len() && position2 < chars.len() {
        let char1 = chars[position1];
        let char2 = chars[position2];

        char1 != char2 && (&char1 == char || &char2 == char)
    } else {
        false
    }
}

fn parse_line(line: &str) -> Option<PasswordEntry> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d+)-(\d+)\s(\w):\s(\w+)$").unwrap();
    }

    let captures = RE.captures(line);
    match captures {
        Some(c) => {
            let lowest_times = c.get(1);
            let highest_times = c.get(2);
            let char = c.get(3);
            let password = c.get(4);

            match (lowest_times, highest_times, char, password) {
                (Some(l), Some(h), Some(c), Some(p)) => {
                    let lowest_times = l.as_str().parse::<usize>().unwrap();
                    let highest_times = h.as_str().parse::<usize>().unwrap();
                    let char = c.as_str().parse::<char>().unwrap();
                    let password = String::from(p.as_str());

                    Some((lowest_times, highest_times, char, password))
                },
                _ => None
            }
        },
        _ => None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_part1_valid_1() {
        let entry = (1usize, 3usize, 'a', String::from("abcde"));

        let valid = is_part1_valid(&entry);

        assert_eq!(valid, true);
    }

    #[test]
    fn is_part1_valid_2() {
        let entry = (1usize, 3usize, 'b', String::from("cdefg"));

        let valid = is_part1_valid(&entry);

        assert_eq!(valid, false);
    }

    #[test]
    fn is_part1_valid_3() {
        let entry = (2usize, 9usize, 'c', String::from("ccccccccc"));

        let valid = is_part1_valid(&entry);

        assert_eq!(valid, true);
    }

    #[test]
    fn part1_aoc_input() {
        let entries = vec![
            (1usize, 3usize, 'a', String::from("abcde")),
            (1usize, 3usize, 'b', String::from("cdefg")),
            (2usize, 9usize, 'c', String::from("ccccccccc")),
        ];

        let count = part1(&entries);

        assert_eq!(count, 2);
    }

    #[test]
    fn is_part2_valid_1() {
        let entry = (1usize, 3usize, 'a', String::from("abcde"));

        let valid = is_part2_valid(&entry);

        assert_eq!(valid, true);
    }

    #[test]
    fn is_part2_valid_2() {
        let entry = (1usize, 3usize, 'b', String::from("cdefg"));

        let valid = is_part2_valid(&entry);

        assert_eq!(valid, false);
    }

    #[test]
    fn is_part2_valid_3() {
        let entry = (2usize, 9usize, 'c', String::from("ccccccccc"));

        let valid = is_part2_valid(&entry);

        assert_eq!(valid, false);
    }

    #[test]
    fn is_part2_valid_4() {
        let entry = (1usize, 2usize, 'a', String::from("ba"));

        let valid = is_part2_valid(&entry);

        assert_eq!(valid, true);
    }

    #[test]
    fn part2_position1_greater_than_length() {
        let entry = (10usize, 2usize, 'a', String::from("ba"));

        let valid = is_part2_valid(&entry);

        assert_eq!(valid, false);
    }

    #[test]
    fn part2_position2_greater_than_length() {
        let entry = (1usize, 20usize, 'a', String::from("ba"));

        let valid = is_part2_valid(&entry);

        assert_eq!(valid, false);
    }

    #[test]
    fn part2_aoc_input() {
        let entries = vec![
            (1usize, 3usize, 'a', String::from("abcde")),
            (1usize, 3usize, 'b', String::from("cdefg")),
            (2usize, 9usize, 'c', String::from("ccccccccc")),
        ];

        let count = part2(&entries);

        assert_eq!(count, 1);
    }

    #[test]
    fn parse_line_returns_correct_result() {
        let line = "13-15 c: scnqcccccccqpgccqccc";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_some(), true);
        assert_eq!(parsed.unwrap(), (13, 15, 'c', String::from("scnqcccccccqpgccqccc")));
    }

    #[test]
    fn parse_line_incorrect_lowest() {
        let line = "a-15 c: scnqcccccccqpgccqccc";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_none(), true);
    }

    #[test]
    fn parse_line_incorrect_highest() {
        let line = "13-bb c: scnqcccccccqpgccqccc";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_none(), true);
    }

    #[test]
    fn parse_line_incorrect_char() {
        let line = "13-15 cadsf: scnqcccccccqpgccqccc";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_none(), true);
    }

    #[test]
    fn parse_line_missing_password() {
        let line = "13-15 c: ";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_none(), true);
    }

    #[test]
    fn parse_line_missing_dash() {
        let line = "1315 c: scnqcccccccqpgccqccc";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_none(), true);
    }

    #[test]
    fn parse_line_missing_colon() {
        let line = "13-15 c scnqcccccccqpgccqccc";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_none(), true);
    }

    #[test]
    fn parse_line_empty_string() {
        let line = "";

        let parsed = parse_line(line);

        assert_eq!(parsed.is_none(), true);
    }
}
