pub fn run(part: &str, input_path: &str) {
    let input = std::fs::read_to_string(input_path).unwrap();
    let numbers = parse_input(&input);

    if part == "part1" {
        let result = part1(&numbers, 25);
        println!("{}", result);
    } else if part == "part2" {
        let result = part2(&numbers, 25);
        println!("{}", result);
    }
}

fn part1(numbers: &Vec<usize>, preamble_size: usize) -> usize {
    for i in preamble_size..numbers.len() {
        if !is_valid(i, numbers, preamble_size) {
            return numbers[i];
        }
    }

    0
}

fn part2(numbers: &Vec<usize>, preamble_size: usize) -> usize {
    let target = part1(numbers, preamble_size);

    for i in 0..numbers.len() {
        let n = numbers[i];
        let mut sum = n;
        let mut set = Vec::new();

        set.push(n);

        for j in (i + 1)..numbers.len() {
            let n = numbers[j];
            sum += n;
            set.push(n);
            if sum == target {
                let min = set
                    .iter()
                    .min()
                    .unwrap();
                let max = set
                    .iter()
                    .max()
                    .unwrap();
                return min + max;
            }
            if sum > target {
                break;
            }
        }
    }

    println!("bye!");
    0
}

fn is_valid(index: usize, numbers: &Vec<usize>, preamble_size: usize) -> bool {
    if index < preamble_size {
        panic!("index must be greater than or equal to preamble");
    }

    let target = numbers[index];

    let begin = index - preamble_size;
    let end = index;
    for i in begin..end {
        for j in (i + 1)..end {
            let x = numbers[i];
            let y = numbers[j];

            if (x + y) == target {
                return true;
            }
        }
    }

    false
}

fn parse_input(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|l| l.parse::<usize>().unwrap())
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_aoc_input() {
        let input = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";

        let numbers = parse_input(input);

        assert_eq!(numbers.len(), 20);
        assert_eq!(numbers[0], 35);
        assert_eq!(numbers[19], 576);
    }

    #[test]
    fn is_valid_1() {
        let input = "1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26";
        let numbers = parse_input(input);

        let is_valid = is_valid(25, &numbers, 25);

        assert!(is_valid);
    }

    #[test]
    fn aoc_input_index_5_is_valid() {
        let input = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
        let numbers = parse_input(input);

        let is_valid = is_valid(5, &numbers, 5);

        assert!(is_valid);
    }

    #[test]
    fn aoc_input_index_6_is_valid() {
        let input = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
        let numbers = parse_input(input);

        let is_valid = is_valid(6, &numbers, 5);

        assert!(is_valid);
    }

    #[test]
    fn aoc_input_index_14_is_invalid() {
        let input = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
        let numbers = parse_input(input);

        let is_valid = is_valid(14, &numbers, 5);

        assert!(!is_valid);
    }

    #[test]
    fn part1_aoc_input() {
        let input = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
        let numbers = parse_input(input);

        let result = part1(&numbers, 5);

        assert_eq!(result, 127);
    }

    #[test]
    fn part2_aoc_input() {
        let input = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
        let numbers = parse_input(input);

        let result = part2(&numbers, 5);

        assert_eq!(result, 62);
    }
}
