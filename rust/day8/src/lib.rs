use std::collections::HashSet;

pub fn run(part: &str, input_path: &str) {
    let input = std::fs::read_to_string(input_path).unwrap();
    let program = parse_input(&input);

    if part == "part1" {
        let accumulator = part1(&program);
        println!("{}", accumulator);
    } else if part == "part2" {
        let accumulator = part2(&program);
        println!("{}", accumulator);
    }
}

fn part1(program: &Vec<(String, i32)>) -> i32 {
    let (accumulator, _) = execute(program);
    accumulator
}

fn part2(program: &Vec<(String, i32)>) -> i32 {
    for i in 0..program.len() {
        let (operation, value) = &program[i];
        if operation != "nop" && operation != "jmp" {
            continue;
        }

        let mut new_program = program.clone();
        new_program[i] = match operation.as_str()
        {
            "nop" => ("jmp".to_string(), *value),
            "jmp" => ("nop".to_string(), *value),
            _ => panic!("Invalid operation to swap"),
        };

        let (accumulator, has_loop) = execute(&new_program);
        if !has_loop {
            return accumulator;
        }
    }

    return 0;
}

fn execute(program: &Vec<(String, i32)>) -> (i32, bool) {
    let mut accumulator = 0;
    let mut current_instruction_index: i32 = 0;
    let mut past_instruction_indexes: HashSet<i32> = HashSet::new();
    let mut has_loop = false;
    loop {
        if current_instruction_index as usize == program.len() {
            break;
        }

        if past_instruction_indexes.contains(&current_instruction_index) {
            has_loop = true;
            break;
        }

        past_instruction_indexes.insert(current_instruction_index);

        let (instruction, value) = &program[current_instruction_index as usize];
        match instruction.as_str() {
            "nop" => {
                current_instruction_index += 1;
            },
            "acc" => {
                accumulator += value;
                current_instruction_index += 1;
            },
            "jmp" => {
                current_instruction_index += value;
            },
            _ => panic!("Invalid instruction"),
        };
    }

    (accumulator, has_loop)
}

fn parse_input(input: &str) -> Vec<(String, i32)> {
    input
        .lines()
        .map(|l| parse_line(l))
        .collect::<Vec<_>>()
}

fn parse_line(line: &str) -> (String, i32) {
    let mut split = line.trim().split(" ").map(|s| s.trim());
    let operation = String::from(split.next().unwrap());
    let value = split.next().unwrap().parse::<i32>().unwrap();

    (operation, value)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_line_positive_value() {
        let line = "nop +0";

        let (operation, value) = parse_line(line);

        assert_eq!(operation.as_str(), "nop");
        assert_eq!(value, 0);
    }

    #[test]
    fn parse_line_negative_value() {
        let line = "acc -99";

        let (operation, value) = parse_line(line);

        assert_eq!(operation.as_str(), "acc");
        assert_eq!(value, -99);
    }

    #[test]
    fn parse_aoc_input() {
        let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";

        let program = parse_input(input);

        assert_eq!(program.len(), 9);
    }

    #[test]
    fn part1_aoc_input() {
        let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        let program = parse_input(input);

        let accumulator = part1(&program);

        assert_eq!(accumulator, 5);
    }

    #[test]
    fn part2_aoc_input() {
        let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        let program = parse_input(input);

        let accumulator = part2(&program);

        assert_eq!(accumulator, 8);
    }
}
